// 菜单 侧边栏
export default [
  { path: '/index', title: 'Home', icon: 'home' },
  {
    title: 'POS management',
    icon: 'folder-o',
    children: [
      { path: '/terminal-storage', title: 'Terminal storage' },
      { path: '/serial-management', title: 'Serial management' },
      { path: '/agent-pos-management', title: 'Agent POS management' }
    ]
  },
  {
    title: 'Transaction management',
    icon: 'folder-o',
    children: [
      { path: '/transaction-record', title: 'Transaction Record' }
    ]
  },
  {
    title: 'iPos',
    icon: 'folder-o',
    children: [
      { path: '/ipos-transactions', title: 'iPos Transactions' },
      { path: '/ipos-cashback', title: 'iPos Cashback' }
    ]
  }
]
