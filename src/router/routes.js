import layoutHeaderAside from '@/layout/header-aside'

// 由于懒加载页面太多的话会造成webpack热更新太慢，所以开发环境不使用懒加载，只有生产环境使用懒加载
const _import = require('@/libs/util.import.' + process.env.NODE_ENV)

/**
 * 在主框架内显示
 */
const frameIn = [
  {
    path: '/',
    redirect: { name: 'index' },
    component: layoutHeaderAside,
    children: [
      // 首页
      {
        path: 'index',
        name: 'index',
        meta: {
          auth: true
        },
        component: _import('system/index')
      },
      {
        path: '/terminal-storage',
        name: 'terminal-storage',
        meta: {
          auth: true,
          cache: true,
          title: 'Terminal storage'
        },
        component: _import('pos-management/terminal-storage/Terminal-storage.vue')
      },
      {
        path: '/terminal-storage-import',
        name: 'terminal-storage-import',
        meta: {
          auth: true,
          cache: true,
          title: 'Terminal storage import'
        },
        component: _import('pos-management/terminal-storage/Terminal-storageImport.vue')
      },
      {
        path: '/agent-pos-management',
        name: 'agent-pos-management',
        meta: {
          auth: true,
          cache: true,
          title: 'Agent POS management'
        },
        component: _import('pos-management/agent-pos-management/Agent-pos-management.vue')
      },
      {
        path: '/serial-management',
        name: 'serial-management',
        meta: {
          auth: true,
          cache: true,
          title: 'Serial management'
        },
        component: _import('pos-management/serial-management/serial-management.vue')
      },
      {
        path: '/agency-detail/:id',
        name: 'agency-detail',
        meta: {
          auth: true,
          cache: true,
          title: 'Agency detail'
        },
        component: _import('pos-management/agent-pos-management/Agency-detail.vue')
      },
      {
        path: '/transaction-record',
        name: 'transaction-record',
        meta: {
          auth: true,
          cache: true,
          title: 'Transaction Record'
        },
        component: _import('transaction-management/transaction-record/Transaction-record.vue')
      },
      {
        path: '/ipos-transactions',
        name: 'ipos-transactions',
        meta: {
          auth: true,
          cache: true,
          title: 'IPos Transactions'
        },
        component: _import('ipos/ipos-transactions/Ipos-transactions.vue')
      },
      {
        path: '/ipos-cashback',
        name: 'ipos-cashback',
        meta: {
          auth: true,
          cache: true,
          title: 'IPos Cashback'
        },
        component: _import('ipos/ipos-transactions/Ipos-cashback.vue')
      },
      // 刷新页面 必须保留
      {
        path: 'refresh',
        name: 'refresh',
        hidden: true,
        component: _import('system/function/refresh')
      },
      // 页面重定向 必须保留
      {
        path: 'redirect/:route*',
        name: 'redirect',
        hidden: true,
        component: _import('system/function/redirect')
      }
    ]
  }
]

/**
 * 在主框架之外显示
 */
const frameOut = [
  // 登录
  {
    path: '/login',
    name: 'login',
    component: _import('system/login')
  }
]

/**
 * 错误页面
 */
const errorPage = [
  {
    path: '*',
    name: '404',
    component: _import('system/error/404')
  }
]

// 导出需要显示菜单的
export const frameInRoutes = frameIn

// 重新组织后导出
export default [
  ...frameIn,
  ...frameOut,
  ...errorPage
]
